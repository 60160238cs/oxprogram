import java.util.Scanner;
public class oxgame {
	static Scanner kb = new Scanner(System.in);
	// hello
	public static void main(String[] args) {
		
		char[][] Board = new char[4][4];
		char turn='X';
		showWelcome();
		setborad(Board);
		for(;;) {
			
			showBoard(Board);
			showTurn(turn);
			input(Board,turn);
			if(checkWin(Board,turn))break;
			if(checkDraw(Board,turn))break;
			turn = changeTurn(turn);
		}

	}
	

	private static boolean checkDraw(char[][] Board, char turn) {
		if(Board[1][1] != '-' && Board[1][2] != '-' && Board[1][3] != '-' &&
				Board[2][1] != '-' && Board[2][2] != '-' && Board[2][3] != '-' &&
				Board[3][1] != '-' && Board[3][2] != '-' && Board[3][3] != '-'
				)
		{
			System.out.println("");
			showBoard(Board);
			showDraw();
			return true;
		}
		return false;
	}

	private static char changeTurn(char turn) {
		if(turn=='X') return'O';
		return 'X';
	}

	private static char[][] setborad(char[][] board) {
		for(int i=1; i<4 ; i++)
		{
			for(int j=1; j<4 ; j++)
			{
				board[i][j] = '-';
			}
		}
		return board;
		
	}

	private static boolean checkWin(char Board[][],char turn) {
		int player=1;
		for(int i=1; i<4 ; i++)
		{
			if(Board[i][1] == Board[i][2] && Board[i][2] == Board[i][3])
			{
				if(Board[i][1] == 'X' || Board[i][1] == 'O')
				{
					player=1;
					if(turn=='O')player=2;
					System.out.println("");
					showBoard(Board);
					showWin("Congratulations! Player "+player+" YOU ARE THE WINNER by horizontal.");
					return true;
				}
				
			}
			else if(Board[1][i] == Board[2][i] && Board[2][i] == Board[3][i])
			{
				if(Board[1][i] == 'X' || Board[1][i] == 'O')
				{
					player=1;
					if(turn=='O')player=2;
					System.out.println("");
					showBoard(Board);
					showWin("Congratulations! Player "+player+" YOU ARE THE WINNER by vertical.");
					return true;
				}
			}
			
		}
		
		if(Board[1][1] == Board[2][2] && Board[2][2] == Board[3][3])
		{
			if(Board[1][1] == 'X' || Board[1][1] == 'O')
			{
				player=1;
				if(player=='O')player=2;
				System.out.println("");
				showBoard(Board);
				showWin("Congratulations! Player "+player+" YOU ARE THE WINNER by Oblique.");
				return true;
			}
		}
		
		return false;
	}
	
	private static void showDraw() {
		System.out.print("Unfortunately, no one win.");
	}

	private static void showWin(String textwin) {
		System.out.print(textwin);
	}

	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}
	
	public static void showBoard(char Board[][]) {
		System.out.println("   1 2 3");
		for(int i=1; i<4 ; i++)
		{
			for(int j=1; j<4 ; j++)
			{
				
				if(j==1)
				{
					if(i!=1)
						System.out.print("\n "+i);
					else
						System.out.print(" "+i);
				}
				System.out.print(" "+Board[i][j]);
			}
		}
		System.out.println("");
	}
	
	public static void showTurn(char turn) {
		System.out.println(turn+" turn");
		
	}
	
	public static char[][] input(char Board[][], char turn) {
		System.out.print("Please input Row Col: ");
		int row,colum;
		boolean error = false;
		
		for(;;)
		{
			row = kb.nextInt();
			colum = kb.nextInt();
			for(int i=1; i<4 ; i++)
			{
				for(int j=1; j<4 ; j++)
				{
					if(i==row && j==colum)
					{
						if(Board[i][j]!='-')
						{
							System.out.println("This channel has already owned!");
							error=true;
						}
						else Board[i][j] = turn;
					}
				}
			}
			
			if(error==true)continue;
			else return Board;
			
		}
		
		
		
	}

}
